import logo from './logo.svg';
import './App.css';
import MainShoeShop from './ShoeShop/MainShoeShop';

function App() {
  return (
    <div className="App" style={{ backgroundColor: "#f2e8e0" }}>
      <MainShoeShop />
    </div>
  );
}

export default App;
