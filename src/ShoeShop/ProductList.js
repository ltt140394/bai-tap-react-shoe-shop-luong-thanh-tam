import React, { Component } from 'react'
import ProductItem from './ProductItem'

export default class
    extends Component {
    render() {
        return (
            <div className="row">
                {this.props.productList.map((item, index) => {
                    return <ProductItem key={index} data={item} handleAddToCart={this.props.handleAddToCart} />
                })}
            </div>
        )
    }
}
