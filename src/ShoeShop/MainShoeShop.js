import React, { Component } from 'react'
import Cart from './Cart.js'
import { dataShoeShop } from "./DataShoeShop.js"
import ProductList from './ProductList.js'
import { findProductIndex } from './utils.js'

export default class MainShoeShop extends Component {
    state = {
        productList: dataShoeShop,
        cart: [],
    }

    handleAddToCart = (product) => {
        let cloneCart = [...this.state.cart];
        let indexInCloneCart = findProductIndex(cloneCart, product.id);
        let indexInProductList = findProductIndex(this.state.productList, product.id);
        if (indexInCloneCart == -1) {
            let cartProduct = { ...product, quantity: 1, totalPrice: this.state.productList[indexInProductList].price };
            cloneCart.push(cartProduct);
            this.setState({ cart: cloneCart });
        } else {
            cloneCart[indexInCloneCart].quantity++;
            cloneCart[indexInCloneCart].totalPrice = cloneCart[indexInCloneCart].price * cloneCart[indexInCloneCart].quantity;
            this.setState({ cart: cloneCart });
        }
    }

    handleRemoveToCart = (productID) => {
        let cloneCart = [...this.state.cart];
        let index = findProductIndex(cloneCart, productID);
        cloneCart.splice(index, 1);
        this.setState({ cart: cloneCart });
    }


    handleUpQuantity = (productID) => {
        let cloneCart = [...this.state.cart];
        let index = findProductIndex(cloneCart, productID);
        cloneCart[index].quantity++;
        cloneCart[index].totalPrice = cloneCart[index].price * cloneCart[index].quantity;
        this.setState({ cart: cloneCart });
    }

    handleDownQuantity = (productID) => {
        let cloneCart = [...this.state.cart];
        let index = findProductIndex(cloneCart, productID);
        cloneCart[index].quantity--;
        cloneCart[index].totalPrice = cloneCart[index].price * cloneCart[index].quantity;
        if (cloneCart[index].quantity < 1) {
            cloneCart.splice(index, 1);
        }
        this.setState({ cart: cloneCart });
    }

    handleSubtotal = (something) => {
        let sum = 0;
        this.state.cart.map((item) => {
            return sum += item.totalPrice;
        });
        return sum;
    }

    handleTotalQuantityItem = (something) => {
        let sum = 0;
        let content = "";
        this.state.cart.map((item) => {
            return sum += item.quantity;
        });

        if (sum > 1) {
            content = sum + " items";
        } else {
            content = sum + " item";
        }
        return content;
    }

    handleConfirmPay = () => {
        let cloneCart = [];
        this.setState({ cart: cloneCart });
        window.location.reload();
    }

    render() {
        return (
            <div className="container py-5 ">
                <h1 className="text-info display-3 mb-5">NEW SHOES</h1>
                <ProductList productList={this.state.productList} handleAddToCart={this.handleAddToCart} />
                <Cart
                    cartList={this.state.cart}
                    handleRemoveToCart={this.handleRemoveToCart}
                    handleUpQuantity={this.handleUpQuantity}
                    handleDownQuantity={this.handleDownQuantity}
                    handleSubtotal={this.handleSubtotal}
                    handleTotalQuantityItem={this.handleTotalQuantityItem}
                    handleConfirmPay={this.handleConfirmPay}
                />
            </div>
        )
    }
}
