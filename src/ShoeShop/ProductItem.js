import React, { Component } from 'react'
import styles from "./shoeShop.css"

export default class ProductItem extends Component {
    render() {
        let { name, price, image } = this.props.data;

        return (
            <div className="col-3 my-3">
                <div className="products card">
                    <img className="card-img-top" src={image} alt="Card image cap" />
                    <div className="card-body py-0 px-4" style={{ height: "200px" }}>
                        <h5 className="card-title h-25 mb-3">{name}</h5>
                        <p className="card-text h-25">${price}</p>
                        <button onClick={() => {
                            this.props.handleAddToCart(this.props.data);
                        }} className="btn btn-warning">Add to Cart</button>
                    </div>
                </div>
            </div>
        )
    }
}
