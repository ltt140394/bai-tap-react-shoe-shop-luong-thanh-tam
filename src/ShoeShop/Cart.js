import React, { Component } from 'react'

export default class Cart extends Component {
    render() {
        return (
            <div className="my-5">
                {/* Button trigger modal */}
                <div style={{ position: "fixed", top: "40px", right: "40px", cursor: "pointer" }} data-toggle="modal" data-target="#exampleModal">
                    <i style={{ fontSize: "60px" }} className="fa-solid fa-cart-shopping" >
                    </i>
                    <div style={{ position: "fixed", top: "25px", right: "26px", width: "35px", height: "35px", borderRadius: "50%", backgroundColor: "red", fontSize: "15px", fontWeight: "bold", lineHeight: "35px", overflow: "hidden" }}> <span>{this.props.handleTotalQuantityItem(1)}</span> </div>
                </div>

                {/* Modal */}
                <div className="modal fade" id="exampleModal" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document" style={{ maxWidth: "830px" }}>
                        <div className="modal-content">
                            <div className="modal-header border-bottom-0">
                                <h1 className="modal-title" id="exampleModalLabel">YOUR CART</h1>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">x</span>
                                </button>
                            </div>

                            {this.props.cartList.length >= 1 ?
                                // Hiện giao diện giỏ hàng khi đã có sản phẩm thêm vào
                                <div className="modal-body p-0">
                                    <table className="table mb-0">
                                        <thead className="font-weight-bold" style={{ fontSize: "19px" }}>
                                            <tr>
                                                <td>Name</td>
                                                <td>Price</td>
                                                <td>Quantity</td>
                                                <td>Action</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.props.cartList.map((item, index) => {
                                                return <tr key={index}>
                                                    <td>{item.name}</td>
                                                    <td>${item?.totalPrice}</td>
                                                    <td>
                                                        <button onClick={() => {
                                                            this.props.handleDownQuantity(item.id);
                                                        }} className="btn btn-light">-</button>
                                                        <span className="mx-2">{item?.quantity}</span>
                                                        <button onClick={() => {
                                                            this.props.handleUpQuantity(item.id);
                                                        }} className="btn btn-light">+</button>
                                                    </td>
                                                    <td>
                                                        <button onClick={() => {
                                                            this.props.handleRemoveToCart(item.id);
                                                        }} className="btn btn-danger">Delete</button>
                                                    </td>
                                                </tr>
                                            })}
                                            <tr>
                                                <td colSpan="4" className="text-right font-weight-bold py-4" style={{ fontSize: "19px", paddingRight: "60px" }}> <span>Subtotal ({this.props.handleTotalQuantityItem(1)}): ${this.props.handleSubtotal(1)}</span> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                :
                                // Hiện giao diện giỏ hàng khi chưa có sản phẩm
                                <div className="p-5">
                                    <h4>Your cart is empty. Come and choose the product you want to add to the cart </h4>
                                </div>}
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                {this.props.cartList.length >= 1 ?
                                    <button onClick={this.props.handleConfirmPay} type="button" className="btn btn-success" data-dismiss="modal">Next to pay</button>
                                    : <></>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
